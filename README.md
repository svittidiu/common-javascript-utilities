## Common JavaScript Utilities
This module contains the class CommonJSUtils, which has simple static utility properties and methods that can be used with both frontend and backend JavaScript projects.

### Available public static properties
* STRING_MAX_CODE_UNITS
* TIMER_MIN_DELAY
* TIMER_MAX_DELAY
* DATE_MIN_MILLISECONDS
* DATE_MAX_MILLISECONDS

### Available public static utility methods
* isBlank
* isNotBlank
* isObject
* isNotObject
* isStringWithContent
* isSafeString
* isStringOfLength
* isStringOfMaxLength
* isObjectWithProperties
* isDateObject
* isArrayWithContent
* clearArray
* isValidTimerDelay
* sleep