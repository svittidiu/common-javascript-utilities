/**
 * Contains static utility properties and methods.
 */
export default class CommonJSUtils {

    constructor() {
        throw new Error("Can't instantiate this class!");
    }

    /**
     * The maximum length for a primitive string in 32-bit systems in JavaScript is 2^28-16 code units. Strings of at most this length are considered safe in a sense that they can be safely presented in virtually any environment.
     * 
     * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/length#description
    */
    static get STRING_MAX_CODE_UNITS() {
        return 268435440;
    }

    /**
     * To make sure negative values are not added as delays for timeouts and intervals.
    */
    static get TIMER_MIN_DELAY() {
        return 0;
    }

    /**
     * Most browsers internally store the delays of timers as 32-bit signed integers. This is the maximum value for such.
     * 
     * https://developer.mozilla.org/en-US/docs/Web/API/setTimeout#maximum_delay_value
    */
    static get TIMER_MAX_DELAY() {
        return 2147483647;
    }

    /**
     * The minimum timestamp that can be presented by a JavaScript {@link Date}-object.
     * 
     * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date}
     * @returns {number} Milliseconds before Unix epoch.
    */
    static get DATE_MIN_MILLISECONDS() {
        return -8640000000000000;
    }

    /**
     * The maximum timestamp that can be presented by a JavaScript {@link Date}-object.
     * 
     * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date}
     * @returns {number} Milliseconds after Unix epoch.
    */
    static get DATE_MAX_MILLISECONDS() {
        return 8640000000000000;
    }

    /**
     * Returns `true` if payload is `undefined` or `null`. Otherwise returns `false`.
     * @param {*} payload Object to be tested.
     * @returns {boolean} `true` or `false`
     */
    static isBlank(payload) {
        return payload === undefined || payload === null;
    }

    /**
     * Returns `true` if payload is not `undefined` and is not `null`. Otherwise returns `false`.
     * @param {*} payload Object to be tested.
     * @returns {boolean} `true` or `false`
     */
    static isNotBlank(payload) {
        return payload !== undefined && payload !== null;
    }

    /**
     * Returns `true` if all of the following criteria are met:
     * - payload is not `undefined`
     * - payload is not `null`
     * - `typeof payload` returns `object`
     * 
     * Otherwise returns `false`.
     * 
     * @param {Object} payload The object to be tested. 
     * @returns {boolean} `true` or `false`.
     */
    static isObject(payload) {
        return CommonJSUtils.isNotBlank && typeof payload === "object";
    }

    /**
     * Returns the reverse 0f {@link CommonJSUtils.isObject()}.
     * 
     * @param {Object} payload The object to be tested.
     * @returns {boolean} `true` or `false`.
     */
    static isNotObject(payload) {
        return !CommonJSUtils.isObject(payload);
    }

    /**
     * Returns `true` if all of the following criteria are matched:
     * - `typeof payload` equals to `"string"`
     * - `payload.trim().length` is greater than 0
     * 
     * Otherwise returns `false`.
     * 
     * @param {string} payload Object to be tested.
     * @returns {boolean} `true` or `false`
     */
    static isStringWithContent(payload) {
        return (
            CommonJSUtils.isNotBlank(payload) &&
            typeof payload === "string" &&
            payload.trim().length > 0
        );
    }

    /**
     * Returns `true` if all of the following criteria are met:
     * - `payload` is not `undefined`
     * - `payload` is not `null`
     * - `typeof payload` returns `"string"`
     * - `payload.length` is at most {@link CommonJSUtils.STRING_MAX_CODE_UNITS}
     * 
     * @param {string} payload The string.
     * @returns {boolean} `true` or `false`
     */
    static isSafeString(payload) {
        return (
            CommonJSUtils.isNotBlank(payload) &&
            typeof payload === "string" &&
            payload.length <= CommonJSUtils.STRING_MAX_CODE_UNITS
        );
    }

    /**
     * Tests if given string is exactly of given length.
     * 
     * @param {string} payload A primitive string.
     * @param {number} length A primitive number, the exptected length of the payload. Length must not be less than zero and must not be greater than {@link CommonJSUtils.STRING_MAX_CODE_UNITS}.
     * @returns {boolean} `true` or `false`
     * @throws {Error} If the length is invalid.
     */
    static isStringOfLength(payload, length) {
        CommonJSUtils.#throwIfLengthNotWithinBoundaries(length);
        return typeof payload === "string" && payload.length === length;
    }

    /**
     * Tests if provided string is at most of provided length.
     * 
     * @param {string} payload The string being tested.
     * @param {number} length The maximum length allowed for this string.
     * @returns {boolean} `true` or `false`.
     * @throws {Error} An error if provided length is out of boundaries.
     */
    static isStringOfMaxLength(payload, length) {
        CommonJSUtils.#throwIfLengthNotWithinBoundaries(length);
        return typeof payload === "string" && payload.length <= length;
    }

    /**
     * Returns `true` if all of the following criteria are matched:
     * - `payload` is not `undefined`
     * - `payload` is not `null`
     * - `typeof payload` equals to `"object"`
     * - `Object.keys(payload).length` is greater than 0
     * 
     * Otherwise returns `false`.
     * 
     * @param {object} payload An object.
     * @returns {boolean} `true` or `false`.
     */
    static isObjectWithProperties(payload) {
        return (
            CommonJSUtils.isNotBlank(payload) &&
            typeof payload === "object" &&
            Object.keys(payload).length > 0
        );
    }

    /**
     * Returns `true` if all of the following criteria are matched:
     * - `payload` is not `undefined`
     * - `payload` is not `null`
     * - `typeof payload` equals to `"object"`
     * - `payload instanceof Date` is `true`
     * - `Object.prototype.toString.call(payload)` equals to `"[object Date]"`
     * 
     * Otherwise returns `false`.
     * 
     * @param {Date} payload Object to be tested.
     * @returns {boolean} `true` or `false`
     */
    static isDateObject(payload) {
        // Must check blankness or will throw an Exception.
        return (
            CommonJSUtils.isNotBlank(payload) &&
            typeof payload === "object" &&
            payload instanceof Date &&
            Object.prototype.toString.call(payload) === "[object Date]"
        );
    }

    /**
     * Returns `true` if all of the following criteria are matched:
     * - `payload` is an array
     * - `payload.length` is greater than 0
     * 
     * Otherwise returns `false`.
     * 
     * @param {array} payload Object to be tested.
     * @returns {boolean} `true` or `false`
     */
    static isArrayWithContent(payload) {
        return (
            Array.isArray(payload) &&
            payload.length > 0
        );
    }

    /**
     * Clears an array of elements. Does nothing if provided object is not an array.
     * 
     * @param {array} payload An array of anykind of elements.
    */
    static clearArray(payload) {
        if (Array.isArray(payload)) {
            payload.splice(0, payload.length);
        }
    }

    /**
     * Tests if given payload is a valid timer (setTimeout, setInterval) delay duration. See corresponding values for more information.
     * 
     * @param {number} payload An integer. Minimum value is {@link CommonJSUtils.TIMER_MIN_DELAY} and maximum value is {@link CommonJSUtils.TIMER_MAX_DELAY}.
     * @returns {boolean} `true` or `false`
     */
    static isValidTimerDelay(payload) {
        return Number.isSafeInteger(payload)
            && payload >= CommonJSUtils.TIMER_MIN_DELAY
            && payload <= CommonJSUtils.TIMER_MAX_DELAY;
    }

    /**
     * Sleep for the time given. Time is expected to be given in milliseconds. The delay is set to {@link CommonJSUtils.TIMER_MIN_DELAY} if {@link Number.isSafeInteger} returns `false` or the delay is less than {@link CommonJSUtils.TIMER_MIN_DELAY} or more than {@link CommonJSUtils.TIMER_MAX_DELAY}.
     * 
     * If you want this function to pause execution, then it must be used with `await`. You can also chain this with the `then` handler.
     * 
     * @param {number} milliseconds Milliseconds to sleep.
     * @returns {Promise} Promise.
     */
    static sleep(milliseconds) {
        return new Promise((resolve, reject) => {
            if (CommonJSUtils.isValidTimerDelay(milliseconds)) {
                setTimeout(resolve, milliseconds);
            } else {
                reject(`Invalid sleep duration. Value must be a primitive JavaScript number. Minimum value is ${CommonJSUtils.TIMER_MIN_DELAY} and maximum value is ${CommonJSUtils.TIMER_MAX_DELAY}.`);
            }
        });
    }

    /**
     * Tests if the length used in validations itself is valid. Length of a string cannot be less than 0 and more than {@link CommonUtils.STRING_MAX_CODE_UNITS}
     * 
     * @param {number} payload  The length of a String used in validations.
     * @throws {Error} If provided length is out of boundaries.
     */
    static #throwIfLengthNotWithinBoundaries(payload) {
        if (!Number.isSafeInteger(payload) ||
            payload < 0 ||
            payload > CommonJSUtils.STRING_MAX_CODE_UNITS) {
            throw new Error(`Invalid length provided. Minimum is 0 and maximum is ${CommonJSUtils.STRING_MAX_CODE_UNITS}.`);
        }
    }
}

/**
 * Returns `true` if payload is `undefined` or `null`. Otherwise returns `false`.
 * 
 * @deprecated
 * @param {*} payload Object to be tested.
 * @returns {boolean} `true` or `false`
 */
export function isBlank(payload) {
    return (
        payload === undefined ||
        payload === null
    );
}

/**
 * Returns `true` if payload is not `undefined` and is not `null`. Otherwise returns `false`.
 * 
 * @deprecated
 * @param {*} payload Object to be tested.
 * @returns {boolean} `true` or `false`
 */
export function isNotBlank(payload) {
    return (
        payload !== undefined &&
        payload !== null
    );
}

/**
 * Returns `true` if all of the following criteria are matched:
 * - `typeof payload` equals to `"string"`
 * - `payload.trim().length` is greater than 0
 * 
 * Otherwise returns `false`.
 * 
 * @deprecated
 * @param {string} payload Object to be tested.
 * @returns {boolean} `true` or `false`
 */
export function isStringWithContent(payload) {
    return (
        isNotBlank(payload) &&
        typeof payload === "string" &&
        payload.trim().length > 0
    );
}

/**
 * Returns `true` if all of the following criteria are matched:
 * - `payload` is not `undefined`
 * - `payload` is not `null`
 * - `typeof payload` equals to `"object"`
 * - `Object.keys(payload).length` is greater than 0
 * 
 * Otherwise returns `false`.
 * 
 * @deprecated
 * @param {object} payload An object.
 * @returns {boolean} `true` or `false`.
 */
export function isObjectWithProperties(payload) {
    return (
        isNotBlank(payload) &&
        typeof payload === "object" &&
        Object.keys(payload).length > 0
    );
}

/**
 * Returns `true` if all of the following criteria are matched:
 * - `payload` is not `undefined`
 * - `payload` is not `null`
 * - `typeof payload` equals to `"object"`
 * - `Object.prototype.toString.call(payload)` equals to `"[object Date]"`
 * 
 * Otherwise returns `false`.
 * 
 * @deprecated
 * @param {Date} payload Object to be tested.
 * @returns {boolean} `true` or `false`
 */
export function isDateObject(payload) {
    // Must check blankness or will throw an Exception.
    return (
        isNotBlank(payload) &&
        typeof payload === "object" &&
        Object.prototype.toString.call(payload) === "[object Date]"
    );
}

/**
 * Returns `true` if all of the following criteria are matched:
 * - `payload` is an array
 * - `payload.length` is greater than 0
 * 
 * Otherwise returns `false`.
 * 
 * @deprecated
 * @param {array} payload Object to be tested.
 * @returns {boolean} `true` or `false`
 */
export function isArrayWithContent(payload) {
    return Array.isArray(payload) &&
        payload.length > 0;
}

/**
 * Clears an array of elements. Does nothing if provided object is not an array.
 * 
 * @deprecated
 * @param {array} payload An array of anykind of elements.
*/
export function clearArray(payload) {
    if (Array.isArray(payload)) {
        payload.splice(0, payload.length);
    }
}

export const minimumSleepTime = 1;
export const maximumSleepTime = 2147483647;

/**
 * Sleep for the time given. Time is expected to be given in milliseconds.
 * 
 * If you want this function to pause execution, then it must be used with `await`.
 * 
 * You can also chain this with the `then` handler.
 * 
 * @deprecated
 * @param {Number} milliseconds Milliseconds to sleep. Set to 0 if given value does not meet the requirements of `Number.isSafeInteger`.
 * @returns {Promise} Promise.
 */
export function sleep(milliseconds) {
    if (
        !Number.isSafeInteger(milliseconds) ||
        milliseconds < minimumSleepTime ||
        milliseconds > maximumSleepTime
    ) {
        milliseconds = 1;
    }
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}
